#modified version of http://www.checkyourlogs.net/?p=21001
Function StartService
{
  Param(
    [string]$ServiceName,
    [int]$SecondsToWait = 30
  )
  $DependentServices = (Get-Service -Name $ServiceName -ErrorAction SilentlyContinue).DependentServices | Where-Object { Get-WmiObject win32_service -Filter "name = '$($_.Name)' and StartMode = 'auto'" }
  If ($DependentServices -ne $null)
  {
    ForEach ($DependentService in $DependentServices)
    {
      StartService $DependentService.Name -ErrorAction SilentlyContinue
    }
  }
  Start-Service -Name $ServiceName -ErrorAction SilentlyContinue
  $ServiceStateCorrect = WaitService $ServiceName "Running" $SecondsToWait
  If ($ServiceStateCorrect -eq "Running" )
  {
    Write-Output "$ServiceName has STARTED successfully"
  }
  else
  {
    Write-Output "!! $ServiceName FAILED TO START !!"
  }
}

Function StopService
{
  Param(
    [string]$ServiceName,
    [int]$SecondsToWait = 30
  )
  if (Get-Service $ServiceName -ErrorAction SilentlyContinue)
  {
    Stop-Service $ServiceName -Force -ErrorAction SilentlyContinue
    $ServiceStateCorrect = WaitService $ServiceName "Stopped" $SecondsToWait

    If ($ServiceStateCorrect -eq "Stopped")
    {
      Write-Output "$ServiceName has STOPPED successfully"
    }
    else
    {
      $ServicePid = Get-WmiObject -Class win32_service  -Filter "Name = '${SeviceName}' and State = 'stop pending'" | Select ProcessId
      Stop-Process -Force -Id $ServicePid
      Write-Output "$ServiceName would not stop so it was forcefully killed."
    }
  }
  else
  {
    Write-Output "Could not find a service with the name $ServiceName"
  }
}

Function WaitService
{
  param (
    [string]$ServiceName,
    [ValidateSet("Running", "StartPending", "Stopped", "StopPending", "ContinuePending", "Paused", "PausePending")][string]$ServiceState,
    [int]$SecondsToWait = 30
  )
  $counter = 0
  $ServiceStateCorrect = $False

  if (Get-Service $ServiceName -ErrorAction SilentlyContinue)
  {
    do
    {
      $counter++
      Start-Sleep -Milliseconds 250
      $serviceInfo = Get-Service $ServiceName
      If ($serviceInfo.Status -eq $ServiceState)
      {
        $ServiceStateCorrect = $True
        Break
      }
      else
      {
        if (($Counter * .250) % 1 -eq 0)
        {
          Write-Host "$($counter * .250)/$SecondsToWait [$($ServiceName)] State is not $ServiceState"
        }
      }
    } until (($counter * .250) -ge $SecondsToWait)
    Return $ServiceStateCorrect
  }
  else
  {
    Write-Output "Could not find a service with the name $ServiceName"
  }
}

Function RestartService
{
  Param(
    [string]$ServiceName
  )
  $result = StopService $ServiceName
  if (!$result)
  {
    WaitService -Name $ServiceName 
  }
  $result = StartService $ServiceName 
  $result
}